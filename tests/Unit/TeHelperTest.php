<?php

namespace Tests\Unit;

use App\Helpers\TeHelper;
use Carbon\Carbon;
use Tests\TestCase;

class TeHelperTest extends TestCase
{
    public function testwillExpireAt() {
    	$due_time = Carbon::parse('2018-01-25 10:00:00');
    	$created_at = Carbon::parse('2018-01-15 10:00:00');

    	$expire_at = TeHelper::willExpireAt($due_time, $created_at);    	

    	$this->assertEquals('2018-01-23 10:00:00', $expire_at);
    }
}
