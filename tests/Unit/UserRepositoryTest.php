<?php

namespace Tests\Unit;

use App\Repository\UserRepository;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    public function testcreateOrUpdate_successful_add() {
    	$user = new User;
    	$user_repo = new UserRepository($user);

    	$request['role'] = env('CUSTOMER_ROLE_ID');
    	$request['name'] = 'Marlito Dungog';
    	$request['company_id'] = 1;
    	$request['department_id'] = 1;
    	$request['email'] = 'marlitobdungog@gmail.com';
    	$request['dob_or_orgid'] = 1;
    	$request['phone'] = '123123';
    	$request['mobile'] = '123123';
    	$request['password'] = 'testpass';
    	$request['consumer_type'] = 'paid';
    	$model = $user_repo->createOrUpdate(0, $request);

    	$is_successful = false;
    	if ($model instanceof User) {
    		$is_successful = true;
    	}

    	$this->assertTrue($is_successful);
    }
}
